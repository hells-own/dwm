/* See LICENSE file for copyright and license details. */

#define MAX(A, B)               ((A) > (B) ? (A) : (B))
#define MIN(A, B)               ((A) < (B) ? (A) : (B))
#define MIN3(A, B, C)           MIN(A, MIN(B, C))
#define BETWEEN(X, A, B)        ((A) <= (X) && (X) <= (B))

#ifndef ENABLE_DEBUG
#    define ENABLE_DEBUG 1
#endif
#if ENABLE_DEBUG
#    define debug(...)  fprintf (stderr, __VA_ARGS__)
#else
#    define debug(...)
#endif

#define LENGTH(X)			     (sizeof X / sizeof X[0])

void die(const char *fmt, ...);
void *ecalloc(size_t nmemb, size_t size);
