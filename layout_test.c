#include <stdio.h>

#include "layout.h"

static struct layout layout = {
    121 * 3,
};

static struct client_type types[] = {
    { 1, 120,  80, 120 },
    { 5, 200, 120, 240 },
};

int main()
{
    uint total_width = 0;
    fputs("\n", stderr);
    search_layout (&layout, types, 2);

    fputs("\n", stderr);
    for (int i = 0; i < 2; ++i)
    {
        total_width += types[i].sel_n_cols * types[i].sel_width;
        fprintf(stderr, "%d %d %d\n", types[i].sel_n_cols, types[i].sel_width, types[i].sel_promotions);
    }
    fprintf(stderr, "%d/%d %f\n", total_width, layout.monitor_width, layout.badness);
    return 0;
}
