#include "layout.h"

#ifndef MIN
#define MIN(A,B) ((A) < (B) ? (A) : (B))
#endif

#ifndef ABS
#define ABS(X) ((X) < 0 ? -(X) : (X))
#endif

#define NO_COLS_BADNESS 100.0
#define BELOW_MIN_BADNESS 100.0
#define BELOW_TARGET_BADNESS 1.0
#define ABOVE_TARGET_BADNESS 2.0
#define STACKED_CLIENTS_BADNESS 1.0
#define PROMOTION_BADNESS -0.2
#define DEMOTION_BADNESS 10.0

#define MAX_DEMOTIONS 1
#define MAX_PROMOTIONS 1

#define ENABLE_DEBUG 0

#if ENABLE_DEBUG
# include <stdio.h>
# define DEBUG(...) fprintf(stderr, __VA_ARGS__)
#else
# define DEBUG(...)
#endif

#define RECORD_BADNESS(ACC,N,WHY,VAL) do {      \
        DEBUG("\t%f: (%d) %s\n", VAL, N, WHY);  \
        ACC += VAL;                             \
    } while (0);

void test_layout (struct layout *layout,
                  struct client_type *types, uint n_types,
                  uint desired_width)
{
	int i;
	float scale, bad;
    scale = ((float) layout->monitor_width) / ((float) desired_width);
	bad = 0.0;
    for (i = 0; i < n_types; ++i)
    {
        DEBUG("%d/%d/%d/%d ",
              types[i].test_n_cols,
              types[i].test_width,
              types[i].test_promotions,
              types[i].n_clients);
    }
    DEBUG("\n");
	for (i = 0; i < n_types; ++i)
	{
		types[i].test_width = scale * types[i].target_width;
        if (types[i].test_promotions < 0)
            RECORD_BADNESS(bad, i, "demotions",
                           DEMOTION_BADNESS * types[i].prio
                           * -types[i].test_promotions);
        if (types[i].test_promotions > 0)
            RECORD_BADNESS(bad, i, "promotions",
                           PROMOTION_BADNESS * types[i].prio
                           * types[i].test_promotions);
		if (types[i].test_n_cols == 0)
		{
			if (types[i].n_clients)
                RECORD_BADNESS(bad, i, "no colums", NO_COLS_BADNESS);
			continue;
		}
		if (types[i].test_width < types[i].min_width)
            RECORD_BADNESS(bad, i, "below min",
                           BELOW_MIN_BADNESS
                           * types[i].prio
                           * types[i].n_clients);
		if (types[i].test_width < types[i].target_width)
			RECORD_BADNESS(bad, i, "below target",
                           BELOW_TARGET_BADNESS
                           * types[i].prio
                           * types[i].n_clients
                           * types[i].target_width
                           / types[i].test_width);
		if (types[i].test_width > types[i].target_width)
            RECORD_BADNESS(bad, i, "above target",
                           ABOVE_TARGET_BADNESS * types[i].prio
                           * types[i].n_clients
                           * types[i].test_width
                           / types[i].target_width);
        if (types[i].n_clients > types[i].test_n_cols)
            RECORD_BADNESS(bad, i, "stacked clients",
                           STACKED_CLIENTS_BADNESS * types[i].prio
                           * (types[i].n_clients / types[i].test_n_cols));
	}
    DEBUG("%f\n\n", bad);
	if (bad < layout->badness || layout->badness < 0.0)
	{
		layout->badness = bad;
        layout->desired_width = desired_width;
		for (i = 0; i < n_types; ++i)
		{
			types[i].sel_n_cols = types[i].test_n_cols;
			types[i].sel_width = types[i].test_width;
            types[i].sel_promotions = types[i].test_promotions;
            types[i].sel_n_clients = types[i].n_clients;
		}
	}
}

void iterate_types (struct layout *layout,
					struct client_type *types, uint n_types, uint i,
					uint max_cols, uint desired_width)
{
	uint n_cols;

	if (i == n_types)
    {
		iterate_relocs (layout, types, n_types, 0, desired_width);
        return;
    }
    for (n_cols = 0;
         n_cols <= max_cols
             && n_cols <= types[i].max_cols;
         n_cols ++)
    {
        types[i].test_n_cols = n_cols;
        iterate_types (layout, types, n_types, i + 1,
                       max_cols - n_cols,
                       desired_width + n_cols * types[i].target_width);
    }
}

void iterate_relocs (struct layout *layout,
                     struct client_type *types, uint n_types, uint i,
                     uint desired_width)
{
    int max_demotions, max_promotions, promotions, next;
    if (i == n_types)
    {
        test_layout (layout, types, n_types, desired_width);
        return;
    }

    max_demotions = MIN(MAX_DEMOTIONS,  types[i].n_clients - types[i].test_n_cols);
    max_promotions = MIN(MAX_PROMOTIONS, types[i].n_clients - types[i].test_n_cols);
    for (promotions = -max_demotions;
         promotions <= max_promotions;
         ++ promotions)
    {
        if ((i == 0 && promotions < 0)
            || (i == n_types - 1 && promotions > 0))
            continue;

        next = i + (promotions < 0 ? -1 : 1);

        types[i].test_promotions = promotions;

        DEBUG("(%d)promote %d x%d (%d) ", i, types[i].n_clients, promotions, ABS(promotions));
        types[i].n_clients    -= ABS(promotions);
        types[next].n_clients += ABS(promotions);

        DEBUG("to %d\n", types[i].n_clients);

        iterate_relocs (layout, types, n_types, i + 1, desired_width);

        types[i].n_clients    += ABS(promotions);
        types[next].n_clients -= ABS(promotions);
    }
}

void search_layout (struct layout *layout, struct client_type *types, uint n_types)
{
	uint i, narrowest_class_width, maxcols_mon, maxcols, total_width;

    layout->n_clients = 0;
    narrowest_class_width = 0 - 1;
    for (i = 0; i < n_types; ++i)
    {
        narrowest_class_width
            = MIN(types[i].min_width, narrowest_class_width);
        layout->n_clients += types[i].n_clients;
    }

    maxcols_mon = layout->monitor_width / narrowest_class_width;
	maxcols = MIN(maxcols_mon, layout->n_clients);

	for (i = 0; i < n_types; ++i)
    {
		types[i].max_cols = MIN(maxcols, types[i].n_clients);
    }

    layout->badness = -1;
    layout->n_cols = 0;
	iterate_types (layout, types, n_types, 0, maxcols, 0.0);

    for (i = 0; i < n_types; ++i)
    {
        layout->n_cols += types[i].sel_n_cols;
    }

    total_width = 0;
    for (int i = 0; i < 2; ++i)
    {
        total_width += types[i].sel_n_cols * types[i].sel_width;
        DEBUG("%d %d %d\n", types[i].sel_n_cols, types[i].sel_width, types[i].sel_promotions);
    }
    DEBUG("\n%d/%d %f\n\n", total_width, layout->monitor_width, layout->badness);
}
