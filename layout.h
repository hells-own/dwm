#ifndef _LAYOUT_H
#define _LAYOUT_H

typedef unsigned int uint;

struct client_type
{
    // const
	uint n_clients;
	uint target_width;
	uint min_width;
	uint max_width;
    float prio;

	// volatile
	uint max_cols;
	uint test_n_cols;
	uint test_width;
    int  test_promotions;

    // layout
	uint sel_n_cols;
	uint sel_width;
    int  sel_promotions;
    uint sel_n_clients;

    uint base_per_col;
    uint n_cols_extra;
};

struct layout
{
    // const
	uint monitor_width;
    uint n_clients;

	// volatile
	float badness;
    uint desired_width;
    uint n_cols;
};

void iterate_types (struct layout *layout,
					struct client_type *types, uint n_types, uint i,
					uint max_cols, uint desired_width);
void iterate_relocs (struct layout *layout,
                     struct client_type *types, uint n_types, uint i,
                     uint desired_width);
void test_layout (struct layout *layout,
                  struct client_type *types, uint n_types,
                  uint desired_width);
void search_layout (struct layout *layout,
                   struct client_type *types, uint n_types);
#endif
