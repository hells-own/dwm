_translate(){
    make $(echo $1 | sed 's/\.c/\.o/')
}

_build(){
    make all
}

_rebuild(){
    make clean all
}

_launch() {
    make clean && make
    lpkg-build -PI
    # make layout_test && ./layout_test
}

_debug ()
{
    _rebuild >/dev/null 2>&1
    echo "$(which gdb) --fullname dwm"
}
