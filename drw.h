/* See LICENSE file for copyright and license details. */

typedef struct {
	Cursor cursor;
} Cur;

typedef struct Fnt {
	Display	    *dpy;
	unsigned int h;
	XftFont	    *xfont;
	FcPattern   *pattern;
	struct Fnt  *next;
} Fnt;

typedef XftColor XClr;

typedef enum { ClrPlain, ClrTile } ClrType;

typedef enum { TileStripeForward } TileType;

enum { ColFg, ColBg, ColHl, Col0 }; /* Clr scheme index */

#define ClrFg ColFg
#define ClrBg ColBg
#define ClrHl ColHl

typedef struct {
	TileType	    tile_type;
	const char	  **tile_plt;
	unsigned int	    tile_plt_n;
	const unsigned int *tile_sel;
	unsigned int	    tile_sel_n;
	unsigned int	    tile_w;
} TileDef;

typedef struct {
	const ClrType  type;
	const char    *plain;
	const TileDef *tile;
} ClrDef;

#define CLR_DEF_PLAIN(NAME)                                                    \
	{                                                                      \
		.type = ClrPlain, .plain = NAME                                \
	}
#define CLR_DEF_TILE(TILE)                                                     \
	{                                                                      \
		.type = ClrTile, .tile = TILE                                  \
	}
#define TILE_DEF_STRF(PLT, SEL, W)                                             \
	{                                                                      \
		.tile_type = TileStripeForward, .tile_plt = PLT,               \
		.tile_plt_n = sizeof (PLT) / sizeof (PLT[0]), .tile_sel = SEL, \
		.tile_sel_n = sizeof (SEL) / sizeof (SEL[0]), .tile_w = W      \
	}

typedef struct {
	ClrType type;
	union {
		XClr   c;
		Pixmap tile;
	} v;
} Clr;

typedef struct {
	const Visual       *visual;
	unsigned int  depth;
	const Clr    	   *c;
	const ClrDef 	   *def;
	size_t  	   len;
} Scheme;

typedef struct {
	unsigned int w, h;
	Display	    *dpy;
	int	     screen;
	Window	     root;
	Drawable     drawable;
	GC	     gc;
	const Scheme	    *scheme;
	Fnt	    *fonts;
} Drw;

/* Drawable abstraction */

Drw *
drw_create (Display	*dpy,
	    int		 screen,
	    Window	 win,
	    unsigned int w,
	    unsigned int h);

void
drw_resize (Drw *drw, unsigned int w, unsigned int h);

void
drw_free (Drw *drw);

/* Fnt abstraction */

Fnt *
drw_fontset_create (Drw *drw, const char *fonts[], size_t fontcount);

void
drw_fontset_free (Fnt *set);

unsigned int
drw_fontset_getwidth (Drw *drw, const char *text);

unsigned int
drw_fontset_getwidth_clamp (Drw *drw, const char *text, unsigned int n);

void
drw_font_getexts (Fnt	       *font,
		  const char   *text,
		  unsigned int	len,
		  unsigned int *w,
		  unsigned int *h);

/* Cursor abstraction */
Cur *
drw_cur_create (Drw *drw, int shape);

void
drw_cur_free (Drw *drw, Cur *cursor);

/* Drawing context manipulation */
void
drw_setfontset (Drw *drw, Fnt *set);

/* Drawing functions */
void
drw_rect (Drw	      *drw,
	  int	       x,
	  int	       y,
	  unsigned int w,
	  unsigned int h,
	  int	       filled,
		  size_t       fg, size_t bg, int invert);

int
drw_text (Drw	      *drw,
	  int	       x,
	  int	       y,
	  unsigned int w,
	  unsigned int h,
	  unsigned int lpad,
	  const char  *text,
	  size_t       fg,
	  size_t       bg,
	  int	       invert);

/* Map functions */
void
drw_map (Drw *drw, Window win, int x, int y, unsigned int w, unsigned int h);

/* Colorscheme abstraction */
void
drw_set_win_border (Drw *drw,
		    Window w,
		    const Scheme **scm,
		    size_t clr);

const Scheme *
drw_scm_alloc_default (Drw *drw, const ClrDef def[], size_t n);

const Scheme *
drw_scm_alloc 	(Drw	     *drw,
		 Window	      ctx,
		 Visual	     *visual,
		 unsigned int depth,
		 const ClrDef def[],
		 size_t       len);

int
drw_scm_create_internal (Drw *drw, Scheme *scm, Window ctx);

void
drw_scm_free (const Scheme **scm);

void
drw_xclr_create (Drw *drw, const Visual *vis, XClr *dest, const char *clrname);

/* Tile functions */
void
drw_tile_create (Drw		    *drw,
		 Window		     ctx,
		 const Visual		    *vis,
		 unsigned int	     depth,
		 Clr		    *dest,
		 TileType	     type,
		 const char	   *const *pltname,
		 unsigned int	     nplt,
		 const unsigned int *sel,
		 unsigned int	     nsel,
		 unsigned int	     w);

unsigned int
drw_tile_sel (TileType	   type,
	      unsigned int n,
	      unsigned int w,
	      unsigned int x,
	      unsigned int y);

void
drw_clr_free (Drw *drw, Clr *c);

void
drw_set_fg (Drw *drw, const Scheme *scm, size_t n);

const XClr *
drw_clr_to_plain (const Clr *clr);
