/* Ser LICENSE file for copyright and license details. */
#include <X11/Xft/Xft.h>
#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "drw.h"
#include "util.h"

#define UTF_INVALID 0xFFFD
#define UTF_SIZ	    4

#define DEBUG	    2

static const unsigned char utfbyte[UTF_SIZ + 1] = {0x80, 0, 0xC0, 0xE0, 0xF0};
static const unsigned char utfmask[UTF_SIZ + 1] = {0xC0, 0x80, 0xE0, 0xF0,
						   0xF8};
static const long	   utfmin[UTF_SIZ + 1]	= {0, 0, 0x80, 0x800, 0x10000};
static const long	   utfmax[UTF_SIZ + 1] = {0x10FFFF, 0x7F, 0x7FF, 0xFFFF,
						  0x10FFFF};

static long
utf8decodebyte (const char c, size_t *i)
{
	for (*i = 0; *i < (UTF_SIZ + 1); ++(*i))
		if (((unsigned char) c & utfmask[*i]) == utfbyte[*i])
			return (unsigned char) c & ~utfmask[*i];
	return 0;
}

static size_t
utf8validate (long *u, size_t i)
{
	if (!BETWEEN (*u, utfmin[i], utfmax[i]) || BETWEEN (*u, 0xD800, 0xDFFF))
		*u = UTF_INVALID;
	for (i = 1; *u > utfmax[i]; ++i)
		;
	return i;
}

static size_t
utf8decode (const char *c, long *u, size_t clen)
{
	size_t i, j, len, type;
	long   udecoded;

	*u = UTF_INVALID;
	if (!clen)
		return 0;
	udecoded = utf8decodebyte (c[0], &len);
	if (!BETWEEN (len, 1, UTF_SIZ))
		return 1;
	for (i = 1, j = 1; i < clen && j < len; ++i, ++j) {
		udecoded = (udecoded << 6) | utf8decodebyte (c[i], &type);
		if (type)
			return j;
	}
	if (j < len)
		return 0;
	*u = udecoded;
	utf8validate (u, len);

	return len;
}

Drw *
drw_create (Display	*dpy,
	    int		 screen,
	    Window	 root,
	    unsigned int w,
	    unsigned int h)
{
	Drw *drw    = ecalloc (1, sizeof (Drw));

	drw->dpy    = dpy;
	drw->screen = screen;
	drw->root   = root;
	drw->w	    = w;
	drw->h	    = h;
	drw->drawable =
	    XCreatePixmap (dpy, root, w, h, DefaultDepth (dpy, screen));
	drw->gc = XCreateGC (dpy, root, 0, NULL);
	XSetLineAttributes (dpy, drw->gc, 1, LineSolid, CapButt, JoinMiter);

	return drw;
}

void
drw_resize (Drw *drw, unsigned int w, unsigned int h)
{
	if (!drw)
		return;

	drw->w = w;
	drw->h = h;
	if (drw->drawable)
		XFreePixmap (drw->dpy, drw->drawable);
	drw->drawable = XCreatePixmap (drw->dpy, drw->root, w, h,
				       DefaultDepth (drw->dpy, drw->screen));
}

void
drw_free (Drw *drw)
{
	XFreePixmap (drw->dpy, drw->drawable);
	XFreeGC (drw->dpy, drw->gc);
	drw_fontset_free (drw->fonts);
	free (drw);
}

/* This function is an implementation detail. Library users should use
 * drw_fontset_create instead.
 */
static Fnt *
xfont_create (Drw *drw, const char *fontname, FcPattern *fontpattern)
{
	Fnt	  *font;
	XftFont	  *xfont   = NULL;
	FcPattern *pattern = NULL;

	if (fontname) {
		/* Using the pattern found at font->xfont->pattern does not
		 * yield the same substitution results as using the pattern
		 * returned by FcNameParse; using the latter results in the
		 * desired fallback behaviour whereas the former just results in
		 * missing-character rectangles being drawn, at least with some
		 * fonts. */
		if (!(xfont =
			  XftFontOpenName (drw->dpy, drw->screen, fontname))) {
			fprintf (stderr,
				 "error, cannot load font from name: '%s'\n",
				 fontname);
			return NULL;
		}
		if (!(pattern = FcNameParse ((FcChar8 *) fontname))) {
			fprintf (
			    stderr,
			    "error, cannot parse font name to pattern: '%s'\n",
			    fontname);
			XftFontClose (drw->dpy, xfont);
			return NULL;
		}
	} else if (fontpattern) {
		if (!(xfont = XftFontOpenPattern (drw->dpy, fontpattern))) {
			fprintf (stderr,
				 "error, cannot load font from pattern.\n");
			return NULL;
		}
	} else {
		die ("no font specified.");
	}

	font	      = ecalloc (1, sizeof (Fnt));
	font->xfont   = xfont;
	font->pattern = pattern;
	font->h	      = xfont->ascent + xfont->descent;
	font->dpy     = drw->dpy;

	return font;
}

static void
xfont_free (Fnt *font)
{
	if (!font)
		return;
	if (font->pattern)
		FcPatternDestroy (font->pattern);
	XftFontClose (font->dpy, font->xfont);
	free (font);
}

Fnt *
drw_fontset_create (Drw *drw, const char *fonts[], size_t fontcount)
{
	Fnt   *cur, *ret = NULL;
	size_t i;

	if (!drw || !fonts)
		return NULL;

	for (i = 1; i <= fontcount; i++) {
		if ((cur = xfont_create (drw, fonts[fontcount - i], NULL))) {
			cur->next = ret;
			ret	  = cur;
		}
	}
	return (drw->fonts = ret);
}

void
drw_fontset_free (Fnt *font)
{
	if (font) {
		drw_fontset_free (font->next);
		xfont_free (font);
	}
}

void
drw_xclr_create (Drw *drw, const Visual *vis, XClr *dest, const char *clrname)
{
	if (!drw || !dest || !clrname)
		return;

	if (!XftColorAllocName (drw->dpy, vis,
				DefaultColormap (drw->dpy, drw->screen),
				clrname, dest))
		die ("error, cannot allocate color '%s'", clrname);
}

/* Wrapper to create color schemes. The caller has to call free(3) on the
 * returned color scheme when done using it. */
const Scheme *
drw_scm_alloc_default (Drw *drw, const ClrDef def[], size_t n)
{
	return drw_scm_alloc (drw, drw->root,
			      DefaultVisual (drw->dpy, drw->screen),
			      DefaultDepth  (drw->dpy, drw->screen),
			      def, n);
}

const Scheme *
drw_scm_alloc (Drw	     *drw,
		 Window	      ctx,
		 Visual	     *vis,
		 unsigned int depth,
		 const ClrDef def[],
		 size_t       len)
{
	size_t  i;
	Scheme *scm;
	Clr    *c;

	scm	    = ecalloc (1, sizeof (Scheme));
	c	    = ecalloc (len, sizeof (Clr));
	*scm = (Scheme) {
		.visual = vis,
		.depth	= depth,
		.c	= c,
		.def	= def,
		.len	= len
	};

	for (i = 0; i < len; ++i)
		switch (def[i].type) {
		case ClrPlain:
			debug ("scm create %lu solid\n", i);
			c[i].type = ClrPlain;
			drw_xclr_create (drw, scm->visual, &c[i].v.c,
					 def[i].plain);
			break;
		case ClrTile:
			debug ("scm create %lu tile\n", i);
			c[i].type = ClrTile;
			drw_tile_create (drw, ctx, scm->visual, scm->depth,
					 &c[i],
					 def[i].tile->tile_type,
					 def[i].tile->tile_plt,
					 def[i].tile->tile_plt_n,
					 def[i].tile->tile_sel,
					 def[i].tile->tile_sel_n,
					 def[i].tile->tile_w);
			break;
		}
	return scm;
}

void
drw_setfontset (Drw *drw, Fnt *set)
{
	if (drw)
		drw->fonts = set;
}

void
drw_rect (Drw	      *drw,
	  int	       x,
	  int	       y,
	  unsigned int w,
	  unsigned int h,
	  int	       filled,
		  size_t       fg, size_t bg, int invert)
{
	if (!drw || !drw->scheme)
		return;

	drw_set_fg (drw, drw->scheme, invert ? bg : fg);

	if (filled)
		XFillRectangle (drw->dpy, drw->drawable, drw->gc, x, y, w, h);
	else
		XDrawRectangle (drw->dpy, drw->drawable, drw->gc, x, y, w - 1,
				h - 1);
}

int
drw_text (Drw	      *drw,
	  int	       x,
	  int	       y,
	  unsigned int w,
	  unsigned int h,
	  unsigned int lpad,
	  const char  *text,
	  size_t fg,
	  size_t bg,
	  int invert)
{
	int	     i, ty, ellipsis_x	  = 0;
	unsigned int tmpw, ew, ellipsis_w = 0, ellipsis_len;
	XftDraw	    *d = NULL;
	Fnt	    *usedfont, *curfont, *nextfont;
	int	     utf8strlen, utf8charlen, render = x || y || w || h;
	long	     utf8codepoint = 0;
	const char  *utf8str;
	FcCharSet   *fccharset;
	FcPattern   *fcpattern;
	FcPattern   *match;
	XftResult    result;
	int	     charexists = 0, overflow = 0;
	/* keep track of a couple codepoints for which we have no match. */
	enum { nomatches_len = 64 };
	static struct {
		long	     codepoint[nomatches_len];
		unsigned int idx;
	} nomatches;
	static unsigned int ellipsis_width = 0;

	if (!drw || (render && (!drw->scheme || !w)) || !text || !drw->fonts)
		return 0;

	if (!render) {
		w = invert ? invert : ~invert;
	} else {
		drw_set_fg (drw, NULL, invert ? fg : bg);
		XFillRectangle (drw->dpy, drw->drawable, drw->gc, x, y, w, h);
		d = XftDrawCreate (drw->dpy, drw->drawable,
				   DefaultVisual (drw->dpy, drw->screen),
				   DefaultColormap (drw->dpy, drw->screen));
		x += lpad;
		w -= lpad;
	}

	usedfont = drw->fonts;
	if (!ellipsis_width && render)
		ellipsis_width = drw_fontset_getwidth (drw, "...");
	while (1) {
		ew = ellipsis_len = utf8strlen = 0;
		utf8str			       = text;
		nextfont		       = NULL;
		while (*text) {
			utf8charlen =
			    utf8decode (text, &utf8codepoint, UTF_SIZ);
			for (curfont = drw->fonts; curfont;
			     curfont = curfont->next) {
				charexists =
				    charexists
				    || XftCharExists (drw->dpy, curfont->xfont,
						      utf8codepoint);
				if (charexists) {
					drw_font_getexts (curfont, text,
							  utf8charlen, &tmpw,
							  NULL);
					if (ew + ellipsis_width <= w) {
						/* keep track where the ellipsis
						 * still fits */
						ellipsis_x   = x + ew;
						ellipsis_w   = w - ew;
						ellipsis_len = utf8strlen;
					}

					if (ew + tmpw > w) {
						overflow = 1;
						/* called from
						 * drw_fontset_getwidth_clamp():
						 * it wants the width AFTER the
						 * overflow
						 */
						if (!render)
							x += tmpw;
						else
							utf8strlen =
							    ellipsis_len;
					} else if (curfont == usedfont) {
						utf8strlen += utf8charlen;
						text	   += utf8charlen;
						ew	   += tmpw;
					} else {
						nextfont = curfont;
					}
					break;
				}
			}

			if (overflow || !charexists || nextfont)
				break;
			else
				charexists = 0;
		}

		if (utf8strlen) {
			if (render) {
				ty = y + (h - usedfont->h) / 2
				   + usedfont->xfont->ascent;
				XftDrawStringUtf8 (
				    d,
				    drw_clr_to_plain (
					    &drw->scheme->c
					    [invert ? ColBg : ColFg]),
				    usedfont->xfont, x, ty,
				    (XftChar8 *) utf8str, utf8strlen);
			}
			x += ew;
			w -= ew;
		}
		if (render && overflow)
			drw_text (drw, ellipsis_x, y, ellipsis_w, h, 0, "...",
				  fg, bg, invert);

		if (!*text || overflow) {
			break;
		} else if (nextfont) {
			charexists = 0;
			usedfont   = nextfont;
		} else {
			/* Regardless of whether or not a fallback font is
			 * found, the character must be drawn. */
			charexists = 1;

			for (i = 0; i < nomatches_len; ++i) {
				/* avoid calling XftFontMatch if we know we
				 * won't find a match */
				if (utf8codepoint == nomatches.codepoint[i])
					goto no_match;
			}

			fccharset = FcCharSetCreate ();
			FcCharSetAddChar (fccharset, utf8codepoint);

			if (!drw->fonts->pattern) {
				/* Refer to the comment in xfont_create for more
				 * information. */
				die (
				    "the first font in the cache must be "
				    "loaded from a font string.");
			}

			fcpattern = FcPatternDuplicate (drw->fonts->pattern);
			FcPatternAddCharSet (fcpattern, FC_CHARSET, fccharset);
			FcPatternAddBool (fcpattern, FC_SCALABLE, FcTrue);

			FcConfigSubstitute (NULL, fcpattern, FcMatchPattern);
			FcDefaultSubstitute (fcpattern);
			match = XftFontMatch (drw->dpy, drw->screen, fcpattern,
					      &result);

			FcCharSetDestroy (fccharset);
			FcPatternDestroy (fcpattern);

			if (match) {
				usedfont = xfont_create (drw, NULL, match);
				if (usedfont
				    && XftCharExists (drw->dpy, usedfont->xfont,
						      utf8codepoint)) {
					for (curfont = drw->fonts;
					     curfont->next;
					     curfont = curfont->next)
						; /* NOP */
					curfont->next = usedfont;
				} else {
					xfont_free (usedfont);
					nomatches.codepoint[++nomatches.idx
							    % nomatches_len] =
					    utf8codepoint;
no_match:
					usedfont = drw->fonts;
				}
			}
		}
	}
	if (d)
		XftDrawDestroy (d);

	return x + (render ? w : 0);
}

void
drw_map (Drw *drw, Window win, int x, int y, unsigned int w, unsigned int h)
{
	if (!drw)
		return;

	XCopyArea (drw->dpy, drw->drawable, win, drw->gc, x, y, w, h, x, y);
	XSync (drw->dpy, False);
}

unsigned int
drw_fontset_getwidth (Drw *drw, const char *text)
{
	if (!drw || !drw->fonts || !text)
		return 0;
	return drw_text (drw, 0, 0, 0, 0, 0, text, 0, 0, 0);
}

unsigned int
drw_fontset_getwidth_clamp (Drw *drw, const char *text, unsigned int n)
{
	unsigned int tmp = 0;
	if (drw && drw->fonts && text && n)
		tmp = drw_text (drw, 0, 0, 0, 0, 0, text, 0, 0, n);
	return MIN (n, tmp);
}

void
drw_font_getexts (Fnt	       *font,
		  const char   *text,
		  unsigned int	len,
		  unsigned int *w,
		  unsigned int *h)
{
	XGlyphInfo ext;

	if (!font || !text)
		return;

	XftTextExtentsUtf8 (font->dpy, font->xfont, (XftChar8 *) text, len,
			    &ext);
	if (w)
		*w = ext.xOff;
	if (h)
		*h = font->h;
}

Cur *
drw_cur_create (Drw *drw, int shape)
{
	Cur *cur;

	if (!drw || !(cur = ecalloc (1, sizeof (Cur))))
		return NULL;

	cur->cursor = XCreateFontCursor (drw->dpy, shape);

	return cur;
}

void
drw_cur_free (Drw *drw, Cur *cursor)
{
	if (!cursor)
		return;

	XFreeCursor (drw->dpy, cursor->cursor);
	free (cursor);
}

void
drw_tile_create (Drw		    *drw,
		 Window		     ctx,
		 const Visual		    *vis,
		 unsigned int	     depth,
		 Clr		    *dest,
		 TileType	     type,
		 const char	   *const *pltnames,
		 unsigned int	     nplt,
		 const unsigned int *sel,
		 unsigned int	     nsel,
		 unsigned int	     w)
{
	Pixmap	     p;
	unsigned int x = 0, y = 0, i, j;
	XClr	    *plt;
	GC	     gc;

	gc = XCreateGC (drw->dpy, ctx, 0, NULL);

	switch (type) {
	case TileStripeForward: x = y = w * nsel; break;
	}

	if (!(drw && x && y
	      && (p = XCreatePixmap (drw->dpy, drw->root, x, y, depth))
	      && (plt = ecalloc (nplt, sizeof (XClr)))))
		return;

	for (i = 0; i < nplt; i++)
		drw_xclr_create (drw, vis, &plt[i], pltnames[i]);

	for (i = 0; i < x; i++) {
		for (j = 0; j < y; j++) {
			XSetForeground (
			    drw->dpy, gc,
			    plt[sel[drw_tile_sel (type, nsel, w, i, j)]].pixel);
			XDrawPoint (drw->dpy, p, gc, i, j);
		}
	}

	dest->type   = ClrTile;
	dest->v.tile = p;
}

unsigned int
drw_tile_sel (TileType	   type,
	      unsigned int n,
	      unsigned int w,
	      unsigned int x,
	      unsigned int y)
{
	switch (type) {
	case TileStripeForward: return ((x + y) / w) % n;
	}
	return 0;
}

void
drw_clr_free (Drw *drw, Clr *clr)
{
	switch (clr->type) {
	case ClrPlain: break;
	case ClrTile: XFreePixmap (drw->dpy, clr->v.tile); break;
	}
}

void
drw_set_fg (Drw *drw, const Scheme *scm, size_t n)
{
	const Clr *c;
	if (scm)
		c = scm->c;
	else
		c = drw->scheme->c;

	switch (c->type) {
	case ClrPlain:
		XSetForeground (drw->dpy, drw->gc, c[n].v.c.pixel);
		XSetFillStyle (drw->dpy, drw->gc, FillSolid);
		break;
	case ClrTile:
		XSetTile (drw->dpy, drw->gc, c[n].v.tile);
		XSetFillStyle (drw->dpy, drw->gc, FillTiled);
		break;
	}
}

const XClr *
drw_clr_to_plain (const Clr *clr)
{
	switch (clr->type) {
	case ClrPlain: return &clr->v.c;
	case ClrTile: die ("cannot draw string with tile");
	}
	return NULL;
}

void
drw_set_win_border (Drw *drw, Window w, const Scheme **dptr, size_t n)
{
	unsigned long	     mask = 0;
	XSetWindowAttributes attr;
	XWindowAttributes    curr;

	const Scheme		    *scm;

	if (*dptr)
		scm = *dptr;
	else
		scm = drw->scheme;

	if (!XGetWindowAttributes (drw->dpy, w, &curr)) {
		debug ("XGetWindowAttributes failed w=%ld\n", w);
		return;
	}
	#if 0
	debug ("visual root=%p window=%p\n",
	       (void *) DefaultVisual (drw->dpy, drw->screen),
	       (void *) curr.visual);
	#endif

	if (curr.visual != scm->visual || curr.depth != scm->depth) {
		if (*dptr)
			drw_scm_free (dptr);
		*dptr = drw_scm_alloc (drw, w, curr.visual, curr.depth,
				       drw->scheme->def, drw->scheme->len);
		scm = *dptr;
	}

	if (curr.class != InputOutput) {
		debug ("window is InputOnly w=%ld\n", w);
		return;
	}

	switch (scm->c[n].type) {
	case ClrPlain:
		mask		  = CWBorderPixel;
		attr.border_pixel = scm->c[n].v.c.pixel;
		break;
	case ClrTile:
		mask		   = CWBorderPixmap;
		attr.border_pixmap = scm->c[n].v.tile;
		break;
	}
	#if 0
	debug ("set_win_border w=%ld\n", w);
	#endif

	XChangeWindowAttributes (drw->dpy, w, mask, &attr);
}

void
drw_scm_free (const Scheme **scm)
{
}
